#include<stdio.h>
#include<stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int		ft_atoi(char *str)
{
	int		i;
	int		res;
	int		sign;

	sign = 1;
	i = 0;
	res = 0;
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\n' || str[i] == '\v'
	|| str[i] == '\r' || str[i] == '\f')
		i++;
	if (str[i] == '-')
	{
		sign = (-1);
		i++;
	}
	if (str[i] < '0' || str[i] > '9')
		return(0);
	while (str[i] >= 48 && str[i] <= 57)
		res = res * 10 + (str[i++] - 48);
	return (sign * res);
}

int     get_tab_cols_amount(char **tab)
{
    int i;

    i = 0;
    while (tab[0][i] != '\0')
        i++;
    return (i);
}

void    display_int_tab(int **tab, int rows, int cols)
{
    int i;
    int j;

    i = 0;
    j = 0;
    printf("display_int_tab");
    while (i < rows)
    {
        while (j < cols)
        {
            printf("%d", tab[i][j]);
            j++;
        }
        printf("\n");
        j = 0;
        i++;
    }
}

void    display_char_tab(char **tab, int rows)
{
    int i;
    int j;

    i = 0;
    j = 0;
    printf("display_char_tab:\n");
    while (i < rows)
    {
        while (tab[i][j] != '\0')
        {
            printf("%c", tab[i][j]);
            j++;
        }
        printf("\n");
        j = 0;
        i++;
    }
}

int     min(int a, int b, int c)
{
    int min;

    //printf("a = %d, b = %d, c = %d\n", a, b, c);
    min = a;
    if (a > b)
        min = b;
    if (c < min)
        min = c;
    //printf("\nmin = %d\n", min);
    return (min);
}

int   *get_start(int max, int max_i, int max_j)
{
	int 	*start;

	start = (int *)malloc(sizeof(int) * 2);
	if (max_i == 0)
		start[0] = 0;
	else
		start[0] = max_i - max + 1;
	if (max_j == 0)
		start[1] = 0;
	else
		start[1] = max_j - max + 1;
    //printf("\nstart i = %d ", max_i - max + 1);
    //printf("  start j = %d", max_j - max + 1);
    return (start);
}

void 	final(char **str, int *start, int *end, char **param)
{
	int i;
	int j;
	char symbol;

	symbol = param[3][0];
	printf("\nstart[0] = %d\n", start[0]);
	printf("start[1] = %d\n", start[1]);
	i = start[0];
	j = start[1];

	printf("\nend[1] = %d\n", end[1]);
	printf("end[2] = %d\n", end[2]);
	while (i < end[1] + 1)
	{
		//printf("i = %d\n", i);
		while (j < end[2] + 1)
		{
			//printf("j = %d\n", j);
			str[i][j] = symbol;
			j++;
		}
		j = 0;
		i++;
	}
	i = 0;
	j = 0;
	//printf("\n");
	while (i < ft_atoi(param[0]))
	{
		printf("str[%d] = %s\n", i, str[i]);
		i++;
	}
}

void    bsq(char **tab, char **param, int cols)
{
    int     i;
    int     j;
    int     max_i_j[3];
    int    *new_tab[ft_atoi(param[0])];

    i = 0;
    j = 0;
    max_i_j[0] = 0;
    max_i_j[1] = 0;
    max_i_j[2] = 0;
    cols = get_tab_cols_amount(tab);
    while (i < ft_atoi(param[0]))
    {
        new_tab[i] = (int *)malloc(sizeof(int) * cols);
        i++;
    }

    i = 0;
    j = 0;

    if (ft_atoi(param[0]) == 1 && cols == 1)
    {
    	if (tab[0][0] == '1')
    		write(1, param[3], 1);
    	else
    		write(1, param[2], 1);
    	write(1, "\n", 1);
    	return;
    }

    while (i < ft_atoi(param[0]))
    {
        if (i == 0)
        {
            while (j < cols)
            {
                new_tab[0][j] = tab[0][j] - '0';
                j++;
            }
        }
        else
        {
            while (j < cols)
            {
                if (j == 0)
                    new_tab[i][j] = tab[i][j] - '0';
                else
                    new_tab[i][j] = 0;
                j++;
            }
        }
        j = 0;
        i++;
    }
    display_int_tab(new_tab, ft_atoi(param[0]), cols);
    i = 1;
    j = 1;

    while (i < ft_atoi(param[0]))
    {
        while (j < cols)
        {
            if (tab[i][j] == '0')
                new_tab[i][j] = 0;
            else
            {
                new_tab[i][j] = min(new_tab[i - 1][j],
                              new_tab[i][j - 1],
                              new_tab[i - 1][j - 1]) + 1;
                if ((new_tab[i][j]) > max_i_j[0])
                {
                    max_i_j[0] = new_tab[i][j];
                    max_i_j[1] = i;
                    max_i_j[2] = j;
                }
            }
            j++;
        }
        j = 1;
        i++;
    }
    display_int_tab(new_tab, ft_atoi(param[0]), cols);
    //printf("max_i = %d max_j = %d", max_i_j[1], max_i_j[2]);
    final(
    	tab,
    	get_start(max_i_j[0], max_i_j[1], max_i_j[2]),
    	max_i_j,
    	param
    	);
}

void	to_file(char *buf)
{
	int fd;
	int b;
	char buf2[2];

	fd = open("file_for_standart_input", O_RDWR);
	while ((b = read(0, buf2, 1)))
	{
		buf2[b] = 0;
		printf("buf2 = %s\n", buf2);
		write(fd, buf2, 1);
	}
	close(fd);
}

int 	first_line_symbol_counter(char *buf)
{
	int counter;

	counter = 0;
	while (buf[counter] != '\n')
		counter++;
	return (counter--);
}

int 	parse_cond_helper(int i, int j, int counter)
{
	if (i == 0)
		return (j < counter - 3);
	return (j < 1);
}

char 	**parse(int fd)
{
	char	**param;
	int		i;
	int		j;
	int 	counter;
	char	buf[14];

	//printf("in parser\n");

	j = 0;
	i = 3; // так как у нас 4 параметра (param[0], param[1]...)
	read(fd, buf, 14); //читаем первую строку
	counter = first_line_symbol_counter(buf); // количество символов в первой строке
	//printf("buf = %s\n", buf);
	//printf("counter = %d\n", counter);
	
	param = (char **)malloc(sizeof(char *) * 4); //выделяем память под массив параметров
	while (i >= 0)
	{
		//printf("i: %d, j: %d\n", i, j);
		if (i == 0)
			param[i] = (char*)malloc(sizeof(char) * (counter - 3 + 1)); 
			// выделяем память под число
		else
			param[i] = (char *)malloc(sizeof(char) + 1);	
			// выделяем память под другие параметры
			// символы пустого, заполненного и закраски 
		while (parse_cond_helper(i, j, counter))
		{
			if (i == 0)
			{
				j = 0;
				//printf("counter - 3 - 1 = %d\n", counter - 3 - 1);
				while (j < counter - 3)
				{
					param[i][j] = buf[j]; 	  // если i == 0, то считываем число
					//printf("param[%d][%d] = %c\n", i, j, param[i][j]);
					j++;
				}
			}
			else
				param[i][j] = buf[counter - (3 - i) - 1]; // иначе 
			j++;
		}
		//printf("i: %d, j: %d\n", i, j);
		param[i][j] = '\0';
		i--;
		j = 0;
	}
	//printf("param[0]: %s\n", param[0]);
	//printf("param[1]: %s\n", param[1]);
	//printf("param[2]: %s\n", param[2]);
	//printf("param[3]: %s\n", param[3]);
	return (param);
}

int count_X(int fd)
{
	char buf[1];
	int j;
	int k;

	j = 0;
	while(read(fd, buf, 1))
	{
		if(*buf != '\n')
		{
			j++;
			k = j;
		}
		else
			j = 0;
	}
	return (k);
} 

void	shift_str_array(char **str, int y)
{
	int 	i;
	char 	*swp;

	i = 0;
	swp = str[y];
	//printf("swp = %s\n", swp);
	while (i + 1 < y)
	{
		str[i] = str[i + 1];
		i++;
	}
	str[i] = swp;
	//printf("str[i] = %s\n", str[i]);
}

int 	count_number_symbols(char *nbr_str)
{
	int count;

	count = 0;
	while (nbr_str[count] >= '0' && nbr_str[count] <= '9')
		count++;
	return (count);
}

int 	is_map_first_line_valid(char *file_name, char *nbr_str)
{
	// int 	i;
	int 	amount_of_symbols_fs;
	int 	amount_of_symbols_nbr;
	char 	buf[14];
	int 	fd;

	fd = open(file_name, O_RDONLY);
	read(fd, buf, 14);
	amount_of_symbols_fs = first_line_symbol_counter(buf);
	amount_of_symbols_nbr = count_number_symbols(nbr_str);
	//printf("amount_of_symbols_fs = %d\n", amount_of_symbols_fs);
	//printf("amount_of_symbols_nbr = %d\n", amount_of_symbols_nbr);
	//printf("amount_of_symbols_fs - amount_of_symbols_nbr = %d\n", amount_of_symbols_fs - amount_of_symbols_nbr);
	if (amount_of_symbols_fs - amount_of_symbols_nbr == 3)
		return (1);
	write(2, "map error\n", 11);
	return (0);
}

char	**str_creation (int x, int y, char *file_name)
{
	char	**str;
	char	buf[1]; // буфер на один символ
	int 	i;		// итератор по количеству строк
	int 	j;		// итератор по символам в строке
	int 	fd;

	i = 0;
	j = 0;
	printf("x = %d, y = %d, %s\n", x, y, file_name);
	str = 0;
	str = (char **)malloc(sizeof(char *) * (y));  // выделяем память под массив строк
	// str[0] = (char *)malloc(sizeof(char) * 14); // выделяем память под нулевую строку
	//printf("file_name = %s\n", file_name);
	fd = open(file_name, O_RDONLY); 
	//printf("read(fd, buf, 1) = %zd\n", read(fd, buf, 1));
	str[0] = (char *)malloc(sizeof(char) * (x + 1));
	while(read(fd, buf, 1))				// пока мы можем читать по 1му символу из файла
	{
		if (*buf != '\n')
			{
				str[i][j] = *buf;		
				j++;
			}
		else
		{
			//printf("i = %d, j = %d \n", i, j);
			str[i][j] = '\0';
			i++;
			str[i] = (char *)malloc(sizeof(char) * (x + 1));
			j = 0;
		}
	}	
	shift_str_array(str, y);
	str[y] = 0;
	
	// if (is_map_valid() != 0)
	// 	return (NULL);

	return (str);
}

void	str_transpiler(char **str, char smb1, char smb2, int rows)
{
	int i;
	int j;

	i = 0;
	j = 0;
	//printf("rows = %d\n", rows);
	while (i < rows)
	{
		while (str[i][j] != '\0')
		{
			//printf("[^] str[%d][%d] = %c\n", i, j, str[i][j]);
			//printf("[^] str[%d][%d] == smb1 = %d\n", i, j, str[i][j] == smb1);
			if (str[i][j] == smb1)
				str[i][j] = '1';
			else if (str[i][j] == smb2)
				str[i][j] = '0';
			j++;
		}
		j = 0;
		i++;
	}
	display_char_tab(str, rows);
}

int 	is_right_symbol(char test, char smb1, char smb2)
{
	if (test != smb1 && test != smb2)
		return (0);
	return (1);
}

int 	is_str_valid(char **str, int rows, char smb1, char smb2)
{
	int i;
	int j;
	int line_count;

	i = 0;
	j = 0;
	line_count = 0;
	while (i < rows)
	{
		while (str[i][j] != '\0')
		{
			//printf("[#] str[%d][%d] = %c\n", i, j, str[i][j]);
			if (is_right_symbol(str[i][j], smb1, smb2) == 0)
				return (0);
			j++;
		}
		if (i == 0)
			line_count = j;
		//printf("[#] str[%d][%d] = %c\n", i, j, str[i][j]);
		if ((i != 0 && j != line_count) || (str[i][j] != '\0'))
			return (0);
		j = 0;
		i++;
	}
	return (1);
}

void	from_st_input (void)
{
	int		fd;
	char	buf[2];
	char	**param;
	char	**str;

	to_file(buf);
	fd = open("file_for_standart_input", O_RDWR);
	printf("fd = %d\n", fd);
	param = parse(fd);
	printf("param[0] = %s\n", param[0]);
	str = str_creation(count_X(fd), ft_atoi(param[0]), "file_for_standart_input");
	display_char_tab(str, ft_atoi(param[0]));
}

void 	from_files (int argc, char **argv)
{
	int		fd;
	char	**str;
	char	**param;
	int 	i;

	i = 1;
	while (i < argc)
	{
		fd = open(argv[i], O_RDONLY);
		param = parse(fd);
		printf("param[0] = %s\n", param[0]);
		if (ft_atoi(param[0]) > 0)
		{
			if (is_map_first_line_valid(argv[i], param[0]) == 1)
			{
				str = str_creation(count_X(fd), ft_atoi(param[0]), argv[i]);
				display_char_tab(str, ft_atoi(param[0]));
				printf("param[1] = %s\n", param[1]);
				if (is_str_valid(str, ft_atoi(param[0]), param[1][0], param[2][0]) == 1)
				{
					str_transpiler(str, param[1][0], param[2][0], ft_atoi(param[0]));
					display_char_tab(str, ft_atoi(param[0]));
					bsq(str, param, get_tab_cols_amount(str));
					close(fd);
					i++;
					free(str);
				}
				else
				{
					// printf("is_str_valid(str, ft_atoi(param[0]), param[1][0], param[2][0]) = %d\n", 
						// is_str_valid(str, ft_atoi(param[0]), param[1][0], param[2][0]));
					write(2, "map error\n", 11);
					i++;
				}
			}
			else
				i++;
		}
		else
		{
			printf("something \n");
			write(2, "map error\n", 11);
			i++;
		}
	}
}

int main (int argc, char **argv)
{
	int fd;

	char *s;
	char *str[2];

	s = (char *)malloc(sizeof(char) * 25);
	s = "file_for_standart_input";
	str[0] = NULL;
	str[1] = s;
	if (argc == 1)
	{
		from_st_input();
		from_files(argc + 1, str);
	}
	else
	{
		//printf("argc: %d\n", argc);
		from_files(argc, argv);
	}
	return (0);
}